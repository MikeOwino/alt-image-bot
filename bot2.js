const TelegramBot = require("node-telegram-bot-api");
const express = require("express");
const bodyParser = require("body-parser");

const token = "5268748619:AAGHo1QI6eNRp_Vw3dor28XtwIqJocmJm10";
const bot = new TelegramBot(token);
const app = express();

app.use(bodyParser.json());

app.post("/" + token, (req, res) => {
  bot.processUpdate(req.body);
  res.sendStatus(200);
});

app.listen(3000, () => {
  console.log("Express server listening on port 3000");
});

bot.setWebHook(
  `https://8443-mikeowino-whatsmyip-d4yk3knfmw2.ws-eu80.gitpod.io/${token}`
);
bot.on("message", (msg) => {
  const chatId = msg.chat.id;
  // Check if the message contains an image link
  if (
    msg.text &&
    (msg.text.startsWith("http://") || msg.text.startsWith("https://"))
  ) {
    // Send a request to the Express.js server to generate alt text for the image
    request.post(
      "https://3001-mikeowino-altimagebot-se35byoj68y.ws-eu80.gitpod.io/generate-alt-text",
      {
        json: {
          imageUrl: msg.text,
        },
      },
      (error, res, body) => {
        if (error) {
          console.error(error);
        } else {
          bot.sendMessage(chatId, body);
        }
      }
    );
  }
});
