const readline = require("readline");
const request = require("request");
const fs = require("fs");
const TelegramBot = require("node-telegram-bot-api");

const token = "5268748619:AAGHo1QI6eNRp_Vw3dor28XtwIqJocmJm10";
const bot = new TelegramBot(token);

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Enter image URL: ", (imageUrl) => {
  request.get(
    `https://alt-text-generator.vercel.app/api/generate?imageUrl=${imageUrl}`,
    (error, response, body) => {
      if (error) {
        console.error(error);
      } else {
        fs.writeFile(
          "alt-text.json",
          JSON.stringify({ altText: body }),
          (err) => {
            if (err) {
              console.error(err);
            } else {
              console.log("Alt text saved to alt-text.json");
              console.log("Alt text sent to telegram");
              const altText = body.replace(/"/g, "");
              bot.sendMessage("298284129", altText);
            }
          }
        );
      }
    }
  );
  rl.close();
});
