const express = require('express');
const request = require('request');

const app = express();

app.post('/generate-alt-text', (req, res) => {
  const imageUrl = req.body.imageUrl;
  request.get(`https://alt-text-generator.vercel.app/api/generate?imageUrl=${imageUrl}`, (error, response, body) => {
    if (error) {
      res.send(error);
    } else {
      res.send(body);
    }
  });
});

app.listen(3001, () => {
  console.log('Alt text generator bot listening on port 3000!');
});
