const readline = require('readline');
const request = require('request');
const fs = require('fs');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Enter image URL: ', (imageUrl) => {
  request.get(`https://alt-text-generator.vercel.app/api/generate?imageUrl=${imageUrl}`, (error, response, body) => {
    if (error) {
      console.error(error);
    } else {
      fs.writeFile('text.json', JSON.stringify({ altText: body }), (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log('Alt text saved to alt-text.json');
        }
      });
    }
  });
  rl.close();
});
